var API_URL = 'https://diversions.constantvzw.org/wiki/api.php';

/**
 * Make an api url with the given parameters
 * @param {object} parameters parameters for the url
 */
function makeApiUrl(parameters) {
  var params = new URLSearchParams();

  for (k in parameters) {
    params.append(k, parameters[k]);
  }

  return API_URL + '?' + params.toString();
}

/**
 * Fetches the given url, parses data as json
 * @param {string} url url to fetch
 */
function getJSON (url) {
  return new Promise(function (resolve, reject) {
    fetch(url, {
      method: "GET"
    }).then(function (response) {
      if (response.ok) {
        response.json().then(resolve); //.catch(reject);
      }
      else {
        reject();
      }
    });//.catch(reject);
  });
}



/**
 * Fetches a list of revisions from the wiki api
 * Returns a promise
 * Promise is revolved with a list of revions:
 * Array<Shape<
 *  comment: string
​ *  parentid: integer
​ *  revid: integer
​​ *  timestamp: string
​​ *  user: string
 * >>
 * @param {string} title Title of the lemma
 */
function getRevisions (title) {
  return new Promise(function (resolve, reject) {
    getJSON(makeApiUrl({
      action: 'query',
      prop: 'revisions',
      rvslots: '*',
      rvprop: ['timestamp', 'user', 'comment', 'ids'].join('|'),
      rvlimit: 500,
      format: 'json',
      'titles': title
    })).then(function (data) {
      try {
        var pages = data['query']['pages'],
            keys = Object.keys(pages),
            page = pages[keys[0]],
            revisions = page['revisions'];

        resolve(revisions);
      }
      catch(e) {
        reject(e);
      }
    });
  });
}

/**
 * Fetches a parsed revision from the api
 * Returns a promise
 * Promise is resolved with a dictionary containing the text
 * and display title of the lemma.
 * @param {int} revid Id of the revision 
 */
function getRevision (revid) {
  return new Promise(function (resolve, reject) {
    getJSON(makeApiUrl({
      action: 'parse',
      oldid: revid,
      format: 'json'
    })).then(function (data) {
      try {
        var title = data['parse']['displaytitle'],
            text = Object.values(data['parse']['text'])[0];
        resolve({ title: title, html: text });
      } catch (e) {
        reject(e);
      }
    });
  });
}


function showRevision (revid) {
  var node = document.querySelector('.revisions--revision[data-revid="'+ revid + '"]'),
      current = document.querySelector('.timeline--entry[data-revid="'+ revid + '"]'),
      previous = document.querySelector('.timeline--entry.active');
  if (previous) {
    previous.classList.remove('active');
  }

  current.classList.add('active');

  node.scrollIntoView();
  if (! node.dataset.loaded) {
    node.dataset.loading = true;
    getRevision(revid).then(function (revision) {
      delete node.dataset.loading;
      node.dataset.loaded = true;
      node.innerHTML = revision.html;
      showRevision(revid);
    });
  }
}


(function () {
  var urlParameters = new URLSearchParams(window.location.search),
      title = urlParameters.get('title');


  if (title) {
    getRevisions(title).then(function (revisions) {
      console.log(revisions);

      for (var i = 0; i < revisions.length; i++) {
        console.log('ik ben' + revisions[i].user );
      }
    });
  }
})();

